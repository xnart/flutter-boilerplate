import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/screen/home.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: Locale("tr"),
      supportedLocales: [Locale("tr")],
      initialRoute: "/",
      getPages: [
        GetPage(name: "/", page: () => HomeScreen()),
      ],
    );
  }
}
